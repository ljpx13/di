package di

import (
	"fmt"
	"reflect"
	"sync"
)

// DefaultContainer is the default implementation of Container.
type DefaultContainer struct {
	resolvers map[reflect.Type]Resolver
	mx        *sync.RWMutex
}

var _ Container = &DefaultContainer{}

// TypeReadOnlyContainer is the reflect.Type for the ReadOnlyContainer
// interface.
var TypeReadOnlyContainer = reflect.TypeOf((*ReadOnlyContainer)(nil)).Elem()

// NewDefaultContainer returns a new DefaultContainer, ready to use.
func NewDefaultContainer() *DefaultContainer {
	return &DefaultContainer{
		resolvers: make(map[reflect.Type]Resolver),
		mx:        &sync.RWMutex{},
	}
}

// Register registers a new dependency in the container.
func (c *DefaultContainer) Register(lifetime Lifetime, factoryFunc interface{}) reflect.Type {
	ff := NewFactoryFunc(factoryFunc)
	resolver := getResolverForLifetime(lifetime, ff)

	c.mx.Lock()
	defer c.mx.Unlock()

	typeProvided := ff.Produces()
	c.resolvers[typeProvided] = resolver

	return typeProvided
}

// Resolve resolves the set of passed in interface pointers using the resolvers
// present in the container.  The operation will return an error immediately if
// any of the references provided cannot be resolved into.
//
// If an error is returned, no guarantees are made about which references are or
// are not valid resolved values.  In general, if Resolve does return an error,
// the consumer should assume that none of the provided references were resolved
// successfully.
func (c *DefaultContainer) Resolve(references ...interface{}) error {
	for _, reference := range references {
		err := c.resolveSingle(reference)
		if err != nil {
			return err
		}
	}

	return nil
}

// ResolveType resolves a single instance of the provided type, returning the
// resolved value as an interface{}.  If the type cannot be resolved, an error
// is returned.
func (c *DefaultContainer) ResolveType(t reflect.Type) (interface{}, error) {
	c.mx.RLock()
	defer c.mx.RUnlock()

	resolver, ok := c.resolvers[t]
	if !ok {
		return nil, fmt.Errorf("dependency missing: type '%v' is not registered in the container", t)
	}

	resolvedValue, err := resolver.Resolve(c)
	if err != nil {
		return nil, err
	}

	return resolvedValue, nil
}

// Fork forks the container into a new container.  This new container is subject
// to the functionality of the InstancePerContainer lifetime.
func (c *DefaultContainer) Fork() Container {
	newContainer := &DefaultContainer{
		resolvers: make(map[reflect.Type]Resolver),
		mx:        &sync.RWMutex{},
	}

	c.mx.RLock()
	defer c.mx.RUnlock()

	for typeProvided, resolver := range c.resolvers {
		clonedResolver := resolver.Clone()
		if clonedResolver == nil {
			continue
		}

		newContainer.resolvers[typeProvided] = clonedResolver
	}

	return newContainer
}

func getResolverForLifetime(lifetime Lifetime, ff *FactoryFunc) Resolver {
	switch lifetime {
	case Singleton:
		return NewSingletonResolver(ff, false)
	case AnchoredSingleton:
		return NewSingletonResolver(ff, true)
	case InstancePerContainer:
		return NewInstancePerContainerResolver(ff)
	case AnchoredInstancePerDependency:
		return NewInstancePerDependencyResolver(ff, true)
	default:
		return NewInstancePerDependencyResolver(ff, false)
	}
}

func (c *DefaultContainer) resolveSingle(reference interface{}) error {
	if reference == nil {
		return fmt.Errorf("invalid resolve destination: nil value provided, assuming T is an interface, pass *T, not T")
	}

	v := reflect.ValueOf(reference)
	t := reflect.TypeOf(reference)

	if t.Kind() != reflect.Ptr || (t.Elem().Kind() != reflect.Interface && t.Elem().Kind() != reflect.Ptr) {
		return fmt.Errorf("invalid resolve destination: expected a type like *T where T is an interface or a pointer, but type was '%v'", t)
	}

	// Special case - resolve this container.
	if t.Elem() == TypeReadOnlyContainer {
		v.Elem().Set(reflect.ValueOf(c))
		return nil
	}

	dependencyType := t.Elem()
	resolvedValue, err := c.ResolveType(dependencyType)
	if err != nil {
		return err
	}

	v.Elem().Set(reflect.ValueOf(resolvedValue))
	return nil
}
