package di

import (
	"bytes"
	"io"
	"sync"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestSingletonResolverResolvesConcurrently(t *testing.T) {
	// Arrange.
	concurrencyCount := 5
	callCount := 0
	buf := bytes.NewBuffer([]byte{})
	factoryFunc := NewFactoryFunc(func() io.Writer {
		callCount++
		return buf
	})

	resolver := NewSingletonResolver(factoryFunc, false)

	outc := make(chan interface{})
	wg := &sync.WaitGroup{}
	wg.Add(concurrencyCount)

	go func() {
		wg.Wait()
		close(outc)
	}()

	closure := func() {
		writer, err := resolver.Resolve(NewContainer())
		test.That(t, err, is.Nil())
		outc <- writer
		wg.Done()
	}

	// Act.
	for i := 0; i < concurrencyCount; i++ {
		go closure()
	}

	// Assert.
	for writer := range outc {
		test.That(t, writer, is.EqualTo(buf))
	}

	test.That(t, callCount, is.EqualTo(1))
}

func TestSingletonResolverCloneNonAnchored(t *testing.T) {
	// Arrange.
	factoryFunc := NewFactoryFunc(func(c ReadOnlyContainer) (io.Writer, error) {
		return bytes.NewBuffer([]byte{}), nil
	})

	resolver := NewSingletonResolver(factoryFunc, false)

	// Act.
	newResolver := resolver.Clone()

	// Assert.
	test.That(t, newResolver, is.EqualTo(resolver))
}

func TestSingletonResolverCloneAnchored(t *testing.T) {
	// Arrange.
	factoryFunc := NewFactoryFunc(func(c ReadOnlyContainer) (io.Writer, error) {
		return bytes.NewBuffer([]byte{}), nil
	})

	resolver := NewSingletonResolver(factoryFunc, true)

	// Act.
	newResolver := resolver.Clone()

	// Assert.
	test.That(t, newResolver, is.Nil())
}
