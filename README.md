![](./icon.png)

# di

Package `di` provides a dependency-injection container with support for
different lifetimes.

![](./diagram.png)
