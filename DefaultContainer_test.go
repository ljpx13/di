package di

import (
	"reflect"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestDefaultContainerReturnsRegisteredType(t *testing.T) {
	// Arrange.
	c := NewContainer()

	// Act.
	typeProvided := c.Register(Singleton, func() *containerTestStruct1 { return nil })

	// Assert.
	test.That(t, typeProvided, is.EqualTo(reflect.TypeOf(&containerTestStruct1{})))
}

func TestDefaultContainerLifetimeResolutions(t *testing.T) {
	// Arrange.
	c := setupDefaultContainerTests()

	// Act.
	var inst11, inst12 containerTestInterface1
	var inst21, inst22 *containerTestStruct2
	var inst31, inst32 containerTestInterface3
	err := c.Resolve(&inst11, &inst12, &inst21, &inst22, &inst31, &inst32)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, inst11, is.EqualTo(inst12))
	test.That(t, inst21, is.EqualTo(inst22))
	test.That(t, inst31, is.NotEqualTo(inst32))
}

func TestDefaultContainerLifetimeResolutionsAfterFork(t *testing.T) {
	// Arrange.
	c1 := setupDefaultContainerTests()

	var inst11 containerTestInterface1
	var inst21 *containerTestStruct2
	var inst31 containerTestInterface3
	err := c1.Resolve(&inst11, &inst21, &inst31)
	test.That(t, err, is.Nil())

	// Act.
	c2 := c1.Fork()

	var inst12 containerTestInterface1
	var inst22 *containerTestStruct2
	var inst32 containerTestInterface3
	err = c2.Resolve(&inst12, &inst22, &inst32)
	test.That(t, err, is.Nil())

	// Assert.
	test.That(t, inst11, is.EqualTo(inst12))
	test.That(t, inst21, is.NotEqualTo(inst22))
	test.That(t, inst31, is.NotEqualTo(inst32))
}

func TestDefaultContainerAnchoredSingletonAfterFork(t *testing.T) {
	// Arrange.
	c1 := setupDefaultContainerTests()

	var inst61 *containerTestStruct6
	err := c1.Resolve(&inst61)
	test.That(t, err, is.Nil())

	// Act.
	c2 := c1.Fork()

	var inst62 *containerTestStruct6
	err = c2.Resolve(&inst62)

	// Assert.
	test.That(t, err, is.NotNil())
}

func TestDefaultContainerAnchoredInstancePerDependencyAfterFork(t *testing.T) {
	// Arrange.
	c1 := setupDefaultContainerTests()

	var inst71 *containerTestStruct7
	err := c1.Resolve(&inst71)
	test.That(t, err, is.Nil())

	// Act.
	c2 := c1.Fork()

	var inst72 *containerTestStruct7
	err = c2.Resolve(&inst72)

	// Assert.
	test.That(t, err, is.NotNil())
}

func TestDefaultContainerResolveIntoNonPointer(t *testing.T) {
	// Arrange.
	c := setupDefaultContainerTests()

	// Act.
	var inst containerTestInterface1
	err := c.Resolve(inst)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("invalid resolve destination: nil value provided, assuming T is an interface, pass *T, not T"))
}

func TestDefaultContainerResolveIntoStruct(t *testing.T) {
	// Arrange.
	c := setupDefaultContainerTests()

	// Act.
	var inst containerTestStruct1
	err := c.Resolve(&inst)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("invalid resolve destination: expected a type like *T where T is an interface or a pointer, but type was '*di.containerTestStruct1'"))
}

func TestDefaultContainerResolveFailureInFactoryFunc(t *testing.T) {
	// Arrange.
	c := setupDefaultContainerTests()

	// Act.
	var inst containerTestInterface4
	err := c.Resolve(&inst)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("failed to resolve 'di.containerTestInterface4': dependency missing: type 'di.containerTestInterface5' is not registered in the container"))
}

func TestDefaultContainerShouldResolveSelf(t *testing.T) {
	// Arrange.
	c := setupDefaultContainerTests()

	// Act.
	var inst *containerTestStruct4
	err := c.Resolve(&inst)

	// Assert.
	test.That(t, err, is.Nil())
}

func setupDefaultContainerTests() *DefaultContainer {
	c := NewDefaultContainer()

	c.Register(Singleton, func() containerTestInterface1 {
		return &containerTestStruct1{}
	})

	c.Register(InstancePerContainer, func() *containerTestStruct2 {
		return &containerTestStruct2{}
	})

	c.Register(InstancePerDependency, func() (containerTestInterface3, error) {
		return &containerTestStruct3{}, nil
	})

	c.Register(InstancePerContainer, func(testInterface5 containerTestInterface5) containerTestInterface4 {
		return &containerTestStruct4{dependency: testInterface5}
	})

	c.Register(InstancePerContainer, func(c ReadOnlyContainer) *containerTestStruct4 {
		return &containerTestStruct4{}
	})

	c.Register(AnchoredSingleton, func(testInterface1 containerTestInterface1) *containerTestStruct6 {
		return &containerTestStruct6{dependency: testInterface1}
	})

	c.Register(AnchoredInstancePerDependency, func(testInterface1 containerTestInterface1) *containerTestStruct7 {
		return &containerTestStruct7{dependency: testInterface1}
	})

	return c
}

// -----------------------------------------------------------------------------

type containerTestInterface1 interface {
	Identifier1() string
}

type containerTestInterface3 interface {
	Identifier3() string
}

type containerTestInterface4 interface {
	Identifier4() string
}

type containerTestInterface5 interface {
	Identifier5() string
}

type containerTestStruct1 struct {
	x bool
}

var _ containerTestInterface1 = &containerTestStruct1{}

func (t *containerTestStruct1) Identifier1() string {
	return "1"
}

type containerTestStruct2 struct {
	x bool
}

func (t *containerTestStruct2) Identifier2() string {
	return "2"
}

type containerTestStruct3 struct {
	x bool
}

var _ containerTestInterface3 = &containerTestStruct3{}

func (t *containerTestStruct3) Identifier3() string {
	return "3"
}

type containerTestStruct4 struct {
	dependency containerTestInterface5
}

var _ containerTestInterface4 = &containerTestStruct4{}

func (t *containerTestStruct4) Identifier4() string {
	return "4"
}

type containerTestStruct6 struct {
	dependency containerTestInterface1
}

type containerTestStruct7 struct {
	dependency containerTestInterface1
}
