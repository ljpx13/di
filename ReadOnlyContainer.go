package di

import "reflect"

// ReadOnlyContainer defines the Resolve methods, which allow consumers of the
// interface to Resolve an instance from the container.
type ReadOnlyContainer interface {
	Resolve(references ...interface{}) error
	ResolveType(t reflect.Type) (interface{}, error)
	Fork() Container
}
