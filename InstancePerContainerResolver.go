package di

// InstancePerContainerResolver wraps SingletonResolver and behaves the same,
// except when calling Clone, where it will instead create a new
// InstancePerContainerResolver with a new underlying SingletonResolver that
// references the same FactoryFunc.
type InstancePerContainerResolver struct {
	singletonResolver *SingletonResolver
}

var _ Resolver = &InstancePerContainerResolver{}

// NewInstancePerContainerResolver creates a new InstancePerContainerResolver with the
// provided factory function.
func NewInstancePerContainerResolver(factoryFunc *FactoryFunc) *InstancePerContainerResolver {
	return &InstancePerContainerResolver{
		singletonResolver: NewSingletonResolver(factoryFunc, false),
	}
}

// Resolve calls to the underlying SingletonResolver.
func (r *InstancePerContainerResolver) Resolve(c ReadOnlyContainer) (interface{}, error) {
	return r.singletonResolver.Resolve(c)
}

// Clone clones the current InstancePerContainerResolver into a new instance.  The
// cached value is not cloned to the new instance.
func (r *InstancePerContainerResolver) Clone() Resolver {
	return &InstancePerContainerResolver{
		singletonResolver: NewSingletonResolver(r.singletonResolver.factoryFunc, false),
	}
}
