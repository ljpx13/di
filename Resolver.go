package di

// Resolver defines the methods that any type capable of manging a factory
// function must implement.
type Resolver interface {
	Resolve(c ReadOnlyContainer) (interface{}, error)
	Clone() Resolver
}
