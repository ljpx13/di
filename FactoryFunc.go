package di

import (
	"fmt"
	"reflect"
)

// FactoryFunc is a container for functions that act as constructors.  It
// validates the provided factory function and extracts the set of dependencies
// needed.  The following examples are all valid types for the provided
// function.
//
// func NewSQLTx(db *sql.DB) (*sql.Tx, error)
// func NewRequestHandler(tx *sql.Tx) *Handler
type FactoryFunc struct {
	v reflect.Value
	t reflect.Type
	d []reflect.Type
}

// TypeError is the reflect.Type for the error type.
var TypeError = reflect.TypeOf((*error)(nil)).Elem()

// NewFactoryFunc creates and returns a new FactoryFunc from the provided
// interface value, which MUST match the shape of the example functions provided
// above.
func NewFactoryFunc(ff interface{}) *FactoryFunc {
	fft := reflect.TypeOf(ff)
	assertValidFactoryFunction(fft)

	return &FactoryFunc{
		v: reflect.ValueOf(ff),
		t: getFactoryFunctionReturnType(fft),
		d: getFactoryFunctionDependencies(fft),
	}
}

// Produces returns the type that this factory function produces.
func (ff *FactoryFunc) Produces() reflect.Type {
	return ff.t
}

// Call calls the underlying factory function.  It uses the provided container
// to resolve the dependencies required by the function.
func (ff *FactoryFunc) Call(c ReadOnlyContainer) (interface{}, error) {
	args := []reflect.Value{}

	if len(ff.d) > 0 {
		refs1 := make([]reflect.Value, len(ff.d))
		for i := 0; i < len(ff.d); i++ {
			refs1[i] = reflect.New(ff.d[i])
		}

		refs2 := make([]interface{}, len(refs1))
		for i := 0; i < len(refs1); i++ {
			refs2[i] = refs1[i].Interface()
		}

		err := c.Resolve(refs2...)
		if err != nil {
			return nil, fmt.Errorf("failed to resolve '%v': %v", ff.t, err)
		}

		args = make([]reflect.Value, len(refs1))
		for i := 0; i < len(refs1); i++ {
			args[i] = refs1[i].Elem()
		}
	}

	out := ff.v.Call(args)
	if len(out) == 2 && !out[1].IsNil() {
		return nil, fmt.Errorf("failed to resolve '%v': %v", ff.t, out[1].Interface().(error))
	}

	if out[0].IsNil() {
		return nil, fmt.Errorf("calling the factory function yielded a nil result when a valid '%v' was expected", ff.t)
	}

	return out[0].Interface(), nil
}

func assertValidFactoryFunction(fft reflect.Type) {
	if fft.Kind() != reflect.Func {
		panic("the provided factory function is not a function")
	}

	if fft.NumOut() < 1 || fft.NumOut() > 2 {
		panic("the provided factory function must return an interface or pointer with an optional error")
	}

	t := fft.Out(0)
	kind := fft.Out(0).Kind()
	if kind != reflect.Interface && kind != reflect.Ptr {
		panic(fmt.Sprintf("the provided factory function returns a value that is neither an interface nor a pointer: '%v'", t))
	}

	if fft.NumOut() == 2 && fft.Out(1) != TypeError {
		panic("the second return type, if provided, must be an error")
	}

	for i := 0; i < fft.NumIn(); i++ {
		t := fft.In(i)
		kind := t.Kind()
		if kind != reflect.Interface && kind != reflect.Ptr {
			panic(fmt.Sprintf("the provided factory function contains input arguments that are neither interfaces nor pointers e.g. '%v'", t))
		}
	}
}

func getFactoryFunctionReturnType(fft reflect.Type) reflect.Type {
	return fft.Out(0)
}

func getFactoryFunctionDependencies(fft reflect.Type) []reflect.Type {
	d := make([]reflect.Type, fft.NumIn())

	for i := 0; i < fft.NumIn(); i++ {
		d[i] = fft.In(i)
	}

	return d
}
