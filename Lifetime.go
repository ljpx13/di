package di

// Lifetime is an enum type that represents the intended lifetime of a resolved
// instance.
type Lifetime int

// The set of available lifetimes.
const (
	// Singleton will resolve an instance the first time it is resolved, but
	// will cache the resolved instance and return this instance for any
	// subsequent resolutions.  If the container is forked, the same instance
	// will still be resolved.
	Singleton Lifetime = iota

	// AnchoredSingleton behaves the same as Singleton, but is not considered
	// when a container is forked.  If a type is registered in a container with
	// AnchoredSingleton, and that container is then forked, the type cannot be
	// resolved from the new container (unless registered again).
	AnchoredSingleton

	// InstancePerContainer works in the same way as Singleton, but the cached
	// instance will not persist in the new container.  The factory function
	// will be called to instantiate a new instance if the type is resolved from
	// the forked container.
	InstancePerContainer

	// InstancePerDependency resolves a new instance every time it is resolved.
	InstancePerDependency

	// AnchoredInstancePerDependency behaves the same as InstancePerDependency,
	// but is not considered when a is container forked.  If a type is
	// registered in a container with AnchoredInstancePerDependency, and that
	// container is then forked, the type cannot be resolved from the new
	// container (unless registered again).
	AnchoredInstancePerDependency
)
