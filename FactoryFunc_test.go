package di

import (
	"fmt"
	"io"
	"reflect"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestFactoryFuncProduces(t *testing.T) {
	// Arrange.
	writerType := reflect.TypeOf((*io.Writer)(nil)).Elem()

	// Act.
	ff := NewFactoryFunc(func() io.Writer { return nil })

	// Assert.
	test.That(t, ff.Produces(), is.EqualTo(writerType))
}

func TestFactoryFuncCallSuccess(t *testing.T) {
	// Arrange.
	c := NewContainer()
	c.Register(Singleton, newFactoryFuncTestType1)
	c.Register(Singleton, newFactoryFuncTestType2)

	// Act.
	var fftt1 *factoryFuncTestType1
	err := c.Resolve(&fftt1)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, fftt1.Do1Thing(), is.EqualTo("abc"))
}

func TestFactoryFuncCallFailure1(t *testing.T) {
	// Arrange.
	c := NewContainer()
	c.Register(Singleton, newFactoryFuncTestType1)

	// Act.
	var fftt1 *factoryFuncTestType1
	err := c.Resolve(&fftt1)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("failed to resolve '*di.factoryFuncTestType1': dependency missing: type '*di.factoryFuncTestType2' is not registered in the container"))
}

func TestFactoryFuncCallFailure2(t *testing.T) {
	// Arrange.
	c := NewContainer()
	c.Register(Singleton, newFactoryFuncTestType3)

	// Act.
	var fftt3 *factoryFuncTestType3
	err := c.Resolve(&fftt3)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("failed to resolve '*di.factoryFuncTestType3': always fails"))
}

func TestFactoryFuncCallFailure3(t *testing.T) {
	// Arrange.
	c := NewContainer()
	c.Register(Singleton, func() *factoryFuncTestType3 { return nil })

	// Act.
	var fftt3 *factoryFuncTestType3
	err := c.Resolve(&fftt3)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("calling the factory function yielded a nil result when a valid '*di.factoryFuncTestType3' was expected"))
}

func TestFactoryFuncPanics(t *testing.T) {
	testCases := []struct {
		ff       interface{}
		expected string
	}{
		{
			ff:       5,
			expected: "the provided factory function is not a function",
		},
		{
			ff:       func() {},
			expected: "the provided factory function must return an interface or pointer with an optional error",
		},
		{
			ff:       func() (io.Writer, io.Reader, error) { return nil, nil, nil },
			expected: "the provided factory function must return an interface or pointer with an optional error",
		},
		{
			ff:       func() (io.Writer, io.Reader) { return nil, nil },
			expected: "the second return type, if provided, must be an error",
		},
		{
			ff:       func() (int, error) { return 0, nil },
			expected: "the provided factory function returns a value that is neither an interface nor a pointer: 'int'",
		},
		{
			ff:       func(int) (io.Writer, error) { return nil, nil },
			expected: "the provided factory function contains input arguments that are neither interfaces nor pointers e.g. 'int'",
		},
	}

	for _, testCase := range testCases {
		panicMsg := func() (panicMsg string) {
			defer func() {
				if r := recover(); r != nil {
					panicMsg = r.(string)
				}
			}()

			NewFactoryFunc(testCase.ff)

			return
		}()

		test.That(t, panicMsg, is.EqualTo(testCase.expected))
	}
}

// -----------------------------------------------------------------------------

type factoryFuncTestType1 struct {
	fftt2 *factoryFuncTestType2
}

func newFactoryFuncTestType1(fftt2 *factoryFuncTestType2) *factoryFuncTestType1 {
	return &factoryFuncTestType1{
		fftt2: fftt2,
	}
}

func (t1 *factoryFuncTestType1) Do1Thing() string {
	return t1.fftt2.Do2Thing()
}

// -----------------------------------------------------------------------------

type factoryFuncTestType2 struct{}

func newFactoryFuncTestType2() *factoryFuncTestType2 {
	return &factoryFuncTestType2{}
}

func (t2 *factoryFuncTestType2) Do2Thing() string {
	return "abc"
}

// -----------------------------------------------------------------------------

type factoryFuncTestType3 struct{}

func newFactoryFuncTestType3() (*factoryFuncTestType3, error) {
	return nil, fmt.Errorf("always fails")
}
