package di

import (
	"bytes"
	"io"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestInstancePerContainerResolverClone(t *testing.T) {
	// Arrange.
	factoryFunc := NewFactoryFunc(func() io.Writer {
		return bytes.NewBuffer([]byte{})
	})

	resolver1 := NewInstancePerContainerResolver(factoryFunc)
	inst1, err := resolver1.Resolve(NewContainer())
	test.That(t, err, is.Nil())

	// Act.
	resolver2 := resolver1.Clone()
	inst2, err := resolver2.Resolve(NewContainer())
	test.That(t, err, is.Nil())

	// Assert.
	test.That(t, inst1, is.NotEqualTo(inst2))
}
