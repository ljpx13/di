package di

import (
	"sync"
)

// SingletonResolver waits to call the underlying factory function until Resolve
// is first called.  After the first call, the same value is returned each time
// without any further calls to the factory function.  SingletonResolver is
// thread-safe.
type SingletonResolver struct {
	factoryFunc *FactoryFunc
	anchored    bool

	cachedValue interface{}
	mx          *sync.RWMutex
}

var _ Resolver = &SingletonResolver{}

// NewSingletonResolver creates a new SingletonResolver with the provided
// factory function.  If anchored is true, Clone will always return nil.
func NewSingletonResolver(factoryFunc *FactoryFunc, anchored bool) *SingletonResolver {
	return &SingletonResolver{
		factoryFunc: factoryFunc,
		anchored:    anchored,

		mx: &sync.RWMutex{},
	}
}

// Resolve resolves a value, either by calling the factory function or by
// using the cache.  The provided container will only be used for resolution if
// a value is not in the cache.  It is otherwise ignored.
func (r *SingletonResolver) Resolve(c ReadOnlyContainer) (interface{}, error) {
	value, ok := func() (interface{}, bool) {
		r.mx.RLock()
		defer r.mx.RUnlock()

		if r.cachedValue == nil {
			return nil, false
		}

		return r.cachedValue, true
	}()

	if ok {
		return value, nil
	}

	r.mx.Lock()
	defer r.mx.Unlock()

	if r.cachedValue != nil {
		return r.cachedValue, nil
	}

	value, err := r.factoryFunc.Call(c)
	if err != nil {
		return nil, err
	}

	r.cachedValue = value
	return value, nil
}

// Clone always returns a reference to the current resolver.
func (r *SingletonResolver) Clone() Resolver {
	if r.anchored {
		return nil
	}

	return r
}
