package di

// InstancePerDependencyResolver will call the provided factory function for
// every call to Resolve.  It is simply a wrapper around a factory function that
// satisfies the Resolver interface.
type InstancePerDependencyResolver struct {
	factoryFunc *FactoryFunc
	anchored    bool
}

var _ Resolver = &InstancePerDependencyResolver{}

// NewInstancePerDependencyResolver creates a new InstancePerDependencyResolver with the provided factory
// function.
func NewInstancePerDependencyResolver(factoryFunc *FactoryFunc, anchored bool) *InstancePerDependencyResolver {
	return &InstancePerDependencyResolver{
		factoryFunc: factoryFunc,
		anchored:    anchored,
	}
}

// Resolve calls the underlying factoryFunc directly.
func (r *InstancePerDependencyResolver) Resolve(c ReadOnlyContainer) (interface{}, error) {
	return r.factoryFunc.Call(c)
}

// Clone always returns a reference to the current resolver.
func (r *InstancePerDependencyResolver) Clone() Resolver {
	if r.anchored {
		return nil
	}

	return r
}
