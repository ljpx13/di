package di

import "reflect"

// Container defines the methods that any dependency-injection container must
// implement.
type Container interface {
	ReadOnlyContainer

	Register(lifetime Lifetime, factoryFunc interface{}) reflect.Type
}

// NewContainer returns a new Container, ready for use.  It is an alias of
// NewDefaultContainer.
func NewContainer() Container {
	return NewDefaultContainer()
}
