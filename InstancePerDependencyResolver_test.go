package di

import (
	"bytes"
	"io"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestInstancePerDependencyResolverResolvesNewInstanceForEachCall(t *testing.T) {
	// Arrange.
	callCount := 0
	factoryFunc := NewFactoryFunc(func() io.Writer {
		callCount++
		return bytes.NewBuffer([]byte{})
	})

	resolver := NewInstancePerDependencyResolver(factoryFunc, false)

	// Act.
	inst1, err := resolver.Resolve(NewContainer())
	test.That(t, err, is.Nil())

	inst2, err := resolver.Resolve(NewContainer())
	test.That(t, err, is.Nil())

	// Assert.
	test.That(t, inst1, is.NotEqualTo(inst2))
}

func TestInstancePerDependencyResolverCloneNonAnchoredReturnsCurrentResolver(t *testing.T) {
	// Arrange.
	factoryFunc := NewFactoryFunc(func() io.Writer {
		return bytes.NewBuffer([]byte{})
	})

	resolver := NewInstancePerDependencyResolver(factoryFunc, false)

	// Act.
	newResolver := resolver.Clone()

	// Assert.
	test.That(t, newResolver, is.EqualTo(resolver))
}

func TestInstancePerDependencyResolveCloneAnchoredReturnsNil(t *testing.T) {
	// Arrange.
	factoryFunc := NewFactoryFunc(func() io.Writer {
		return bytes.NewBuffer([]byte{})
	})

	resolver := NewInstancePerDependencyResolver(factoryFunc, true)

	// Act.
	newResolver := resolver.Clone()

	// Assert.
	test.That(t, newResolver, is.EqualTo(nil))
}
