package di

import (
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func BenchmarkDummyDIScenario(b *testing.B) {
	for n := 0; n < b.N; n++ {
		c := setupBenchmarkContainer()

		var db dummyDB
		var h1 dummyHandler
		var h2 dummyHandler
		err := c.Resolve(&db, &h1, &h2)
		test.That(b, err, is.Nil())

		h1.Act(5)
		h2.Act(6)
		h2.Commit()
		h1.Commit()
		test.That(b, db.GetValue("key"), is.EqualTo(6))

		c1 := c.Fork()
		c2 := c.Fork()

		err = c1.Resolve(&h1)
		test.That(b, err, is.Nil())

		err = c2.Resolve(&h2)
		test.That(b, err, is.Nil())

		h1.Act(5)
		h2.Act(6)
		h2.Commit()
		h1.Commit()
		test.That(b, db.GetValue("key"), is.EqualTo(5))
	}
}

func setupBenchmarkContainer() Container {
	c := NewContainer()

	c.Register(Singleton, func() dummyDB {
		return newDefaultDummyDB()
	})

	c.Register(InstancePerContainer, func(db dummyDB) dummyTx {
		return newDefaultDummyTx(db)
	})

	c.Register(InstancePerDependency, func(tx dummyTx) dummyHandler {
		return newDefaultDummyHandler(tx)
	})

	return c
}

// -----------------------------------------------------------------------------

type dummyDB interface {
	SetValue(k string, v int)
	GetValue(k string) int
}

type defaultDummyDB struct {
	values map[string]int
}

var _ dummyDB = &defaultDummyDB{}

func newDefaultDummyDB() *defaultDummyDB {
	return &defaultDummyDB{
		values: map[string]int{},
	}
}

func (d *defaultDummyDB) SetValue(k string, v int) {
	d.values[k] = v
}

func (d *defaultDummyDB) GetValue(k string) int {
	v, _ := d.values[k]
	return v
}

// -----------------------------------------------------------------------------

type dummyTx interface {
	SetValue(k string, v int)
	Commit()
}

type defaultDummyTx struct {
	db      dummyDB
	changes map[string]int
}

var _ dummyTx = &defaultDummyTx{}

func newDefaultDummyTx(db dummyDB) *defaultDummyTx {
	return &defaultDummyTx{
		db:      db,
		changes: map[string]int{},
	}
}

func (d *defaultDummyTx) SetValue(k string, v int) {
	d.changes[k] = v
}

func (d *defaultDummyTx) Commit() {
	for k, v := range d.changes {
		d.db.SetValue(k, v)
	}
}

// -----------------------------------------------------------------------------

type dummyHandler interface {
	Act(v int)
	Commit()
}

type defaultDummyHandler struct {
	tx dummyTx
}

var _ dummyHandler = &defaultDummyHandler{}

func newDefaultDummyHandler(tx dummyTx) *defaultDummyHandler {
	return &defaultDummyHandler{
		tx: tx,
	}
}

func (d *defaultDummyHandler) Act(v int) {
	d.tx.SetValue("key", v)
}

func (d *defaultDummyHandler) Commit() {
	d.tx.Commit()
}
